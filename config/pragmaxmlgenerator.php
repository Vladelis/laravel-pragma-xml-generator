<?php

return [
    'name' => 'Company Name',
    'reg_number' => '123456',
    'vat_reg_number' => 'LT12345678',
    'contract_id' => '123123',
    'email' => 'info@email.com',
    'postal_address' => 'Street st. 145A LT-50131',
    'city' => 'Kaunas',
    'pay_to_account' => 'LT12345678901234567890',
    'pay_to_name' => 'Company Name',
    'invoice_prefix' => 'TES',
    'context' => 'PVM SĄSKAITA FAKTŪRA',
    'description' => 'Sąskaitos apmokėjimas',
    'warehouse_id' => '4',
    'warehouse_returns_id' => '5'
];